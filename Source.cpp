#include <iostream> // allows for input and output
#include <string> // lets me use get line

using namespace std; // lets you use just cout instead of std::cout

string choice; // holds the users choice
string* pointChoice = &choice; // points to the users choice
int grade = 0; // stores the grade

void Options(); // initializes the function
void FirstChoice(); // initializes the function
void VariableTest(int var); // initializes the function
void ReferenceTest(int& ref); // initializes the function
void PointerTest(int* pnt); // initializes the function
void GetNumbersFromUser(); // initializes the function

void main() // A function
{
	cout << "\n \t \t \t \t Welcome to the automated text system\n\n"; // text output
	cout << "\t \t If you would like to type with a representative sorry I'm all you got!\n\n"; // text output
	do // runs once then runs until while is true
	{
		Options(); // runs the function
		GetNumbersFromUser(); // runs the function
	}
	while (choice != "3"); // runs until choice equals three
}

void Options() // A function
{
	cout << "\t \t \t \t Press 1 if you are here to grade\n"; // text output
	cout << "\t \t \t \t Press 2 if you are here to learn\n"; // text output
	cout << "\t \t \t \t Press 3 if you are here to leave\n"; // text output
	grade = 0; // resets grade's value
}

void FirstChoice () // A function
{
	if (choice == "1") // if choice equals 1
	{
		cout << "\t \t \t  This would be my grade if I used only variables "; // text output
		VariableTest(grade); // runs the function
		cout << grade << "\n\n"; // outputs grade
		
		cout << "\t \t \t This might be my grade if I used some references "; // text output
		ReferenceTest(grade); // runs the function
		cout << grade << "\n\n"; // outputs grade
		
		cout << "\t \t \t   This is hopefully my grade using pointers "; // text output
		PointerTest(&grade); // runs the function
		cout << grade << "\n\n"; // outputs grade
	}
	else if (choice == "2") // if choice equals 2
	{
		cout << "\t \t \tThe function - void VariableTest(int var){var = 5;}\n\n"; // text output
		cout << "\t \t \t    Creates a copy of the variable you give it\n"; // text output
		cout << "\t \t So when you change the value it changes the copy not the original\n"; // text output
		cout << "\t    That means that even though the code changes the value to 5 it will still be 0\n\n\n"; // text output
		cout << "\t \t \t \t   The int grade is currently " << grade << "\n\n"; // outputs grade
		VariableTest(grade); // runs the function
		cout << "\t \t \t  After the function that changes it to 5 it is "; // text output
		cout << grade << "\n\n\n\n"; // outputs grade

		cout << "\t \t \tThe function - void ReferenceTest(int& ref){ref = 25;}\n\n"; // text output
		cout << "\t \t      References the original variable allowing it to be changed\n\n\n"; // text output
		cout << "\t \t \t \t   The int grade is currently " << grade << "\n\n"; // outputs grade
		ReferenceTest(grade); // runs the function
		cout << "\t \t \t After the function that changes it to 25 it is "; // text output
		cout << grade << "\n\n\n\n"; // outputs grade

		cout << "\t \t \tThe function - void PointerTest(int* pnt){*pnt = 100;}\n\n"; // text output
		cout << "\t \t \tPoints to the location that the int grade is stored at\n"; // text output
		cout << "\t \t  When you de-reference it you can change the value at that location\n\n\n"; // text output
		cout << "\t \t \t \t   The int grade is currently " << grade << "\n\n"; // outputs grade
		PointerTest(&grade); // runs the function
		cout << "\t \t \t After the function that changes it to 100 it is "; // text output
		cout << grade << "\n\n\n\n"; // outputs grade

		cout << "\t   I hope that that has helped you understand a little bit about references and pointers\n\n"; // text output
	}
	else // if choice equals anything else
	{
		// This exits the program
	}
}

void VariableTest(int var) // A function
{
	var = 5; // sets var to 5
}

void ReferenceTest(int& ref) // A function
{
	ref = 25; // sets ref to 25
}

void PointerTest(int* pnt) // A function
{
	*pnt = 100; // sets pnt to 100
}

void GetNumbersFromUser() // A function
{
	getline(cin, choice); // gets the users input and stores it in choice
	try // runs the code and if it errors runs the catch
	{
		int const temp = stoi(choice); // checks if its a number and stores it in a temp variable
		 
		if (temp == 1 || temp == 2 || temp == 3) // checks if the temp variable equals one or two or three
		{
			cout << "\t\t\tYou picked " << *pointChoice << ". Did you know that's stored at " << &pointChoice << "?\n\n\n\n"; // outputs the choice the user made and tells them where that choice is stored
			FirstChoice(); // runs the function
		}
		else
		{
			stoi("Invalid Number"); // forces a error making it so you can only chose one two or three
		}
	}
	catch (...) // runs if there is an error in the try
	{
		cout << "Input must be a valid number Try Again!\n"; // text output
		GetNumbersFromUser(); // runs the function
	}
}